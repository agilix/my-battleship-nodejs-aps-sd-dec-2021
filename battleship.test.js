const Battleship = require("./battleship");
const Ship = require("./GameController/ship");
const colors = require("cli-color");
const position = require("./GameController/position.js");
const letters = require("./GameController/letters.js");


let inputs = ['A1','A2'].reverse();

class TestableBattleship  extends  Battleship{

    GetUserInput () {
        return inputs.pop();
    }

}

test("place ship", () => {

    const game = new TestableBattleship()

    const ship = new Ship("Patrol Boat", 2, colors.Orange)

    game.PlaceShip(ship)

    expect(ship.positions).toEqual([new position(letters.A, 1), new position(letters.A, 2)]);

})